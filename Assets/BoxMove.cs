﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMove : MonoBehaviour {

    private Rigidbody _rigidbody;

	// Use this for initialization
	void Start () {
        _rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.UpArrow))
            //transform.position += Time.deltaTime * 15.0f * transform.forward;
             _rigidbody.AddForce(transform.forward * 150.0f);

        if (Input.GetKey(KeyCode.RightArrow))
            _rigidbody.AddTorque(transform.up * 100.0f);
            //transform.Rotate(Vector3.up * Time.deltaTime * 200.0f);
        else if (Input.GetKey(KeyCode.LeftArrow))
            _rigidbody.AddTorque(transform.up * -100.0f);
            //transform.Rotate(Vector3.up * Time.deltaTime * -200.0f);
    }
}
