﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public Renderer m_renderer;
    public Collider m_collider;

    public float m_powerBarFillPercentOnPickup = 0.1f;
    //public int m_multiplierToAdd = 1;

	public void OnCollected()
    {
        m_renderer.enabled = false;
        m_collider.enabled = false;

        //Obstacle o = GetComponentInParent<Obstacle>();
        //if (o != null)
        //    o.m_player.AddScore(m_powerBarFillPercentOnPickup);

        //GameManager.Inst.AddMultiplier(m_multiplierToAdd);

        //Player.Inst.AddPowerVal(m_powerBarFillPercentOnPickup);
        Player.Inst.AddPowerPercent(m_powerBarFillPercentOnPickup);
    }

    public void OnReset()
    {
        m_renderer.enabled = true;
        m_collider.enabled = true;
    }

    public void OnPickupReset()
    {
        //m_renderer.enabled = true;
        //m_collider.enabled = true;
    }
}
