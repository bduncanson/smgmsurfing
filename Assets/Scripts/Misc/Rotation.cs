﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {


    public enum Axis { None, X, Y, Z}
    public Axis m_axis = Axis.X;

    public float m_speed = 10.0f;
    public bool m_active = true;

    void Update () {

        if (m_active)
        {
            if (m_axis == Axis.X) {
                transform.eulerAngles += Vector3.right * Time.deltaTime * m_speed; 
            }
            else if (m_axis == Axis.Y) {
                transform.eulerAngles += Vector3.up * Time.deltaTime * m_speed;
            }
            else if (m_axis == Axis.Z) {
                transform.eulerAngles += Vector3.forward * Time.deltaTime * m_speed;
            }
        }
	}
}
