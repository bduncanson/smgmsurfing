﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleCameras : MonoBehaviour {

    public Text m_text;
    
    public GameObject[] m_cameraObjs;

    private int _activeCam = 0;

    public int m_initcam = 3;

    private void Start()
    {
        ChangeCamera(m_initcam);
    }

    public void ChangeCamera(int cam = -1)
    {
        int lastCam = _activeCam;

        if (cam == -1)
        {
            _activeCam++;

            if (_activeCam >= m_cameraObjs.Length)
                _activeCam = 0;
        }
        else
            _activeCam = cam;

        m_cameraObjs[_activeCam].SetActive(true);
        m_cameraObjs[lastCam].SetActive(false);

        m_text.text = m_cameraObjs[_activeCam].name;
    }
}
