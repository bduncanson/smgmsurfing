﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Surfboard : MonoBehaviour
{

    public Player m_player;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Obstacle")) {
            if (m_player) {
                m_player.Reset();
            }
        }
        else if (collider.CompareTag("Coin"))
        {
            collider.GetComponent<Pickup>().OnCollected();
        }
    }
}
