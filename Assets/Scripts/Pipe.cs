﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

    public Player m_player;
    private Transform _transform;
    private Vector3 _defaultPosition;

    public float m_minPos = 5.0f;
    public float m_maxPos = 35.0f;

    public float m_waveSpeed = 25.0f;
    public float m_forwardSpeedFalloff = 0.8f;
    public float m_reverseSpeedFalloff = 0.3f;

    /// from 5 -> 20 -> 35

	void Start () {
        _transform = GetComponent<Transform>();
        _defaultPosition = _transform.position;
	}

    void Update()
    {
        if (!GameManager.Inst.IsRunActive())
            return;

        float waveSpeed = m_waveSpeed;  
        float moveSpeed = waveSpeed - m_player.PlayerSpeed();

        if (moveSpeed <= 0f) {
            moveSpeed *= m_forwardSpeedFalloff;
        }
        else { 
            moveSpeed *= m_reverseSpeedFalloff;
        }

        MoveWave(moveSpeed);
    }

    private void MoveWave(float speed) {
        Vector3 newPos = _transform.position;
        newPos.x = Mathf.Clamp(newPos.x + Time.deltaTime * speed, m_minPos, m_maxPos);
        _transform.position = newPos;
    }

    public void OnReset() {
        _transform.position = _defaultPosition;
    }
}
