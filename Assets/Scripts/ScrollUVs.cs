﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollUVs : MonoBehaviour {

    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1.0f, 0.0f);
    public Vector2 uvAnimationRateUnmodified = new Vector2(0f, 0f);
    public string textureName = "_MainTex";
    public Player m_player; 

    Vector2 uvOffset = Vector2.zero;



    public Renderer m_renderer;

    private void Awake()
    {
        if (m_renderer == null)
            m_renderer = GetComponent<Renderer>();
    }

    void LateUpdate()
    {
        if (m_player)
            uvOffset += (uvAnimationRate * Time.deltaTime * m_player.PlayerSpeed()) + uvAnimationRateUnmodified * Time.deltaTime; 
        else
            uvOffset += (uvAnimationRate * Time.deltaTime);

        if (m_renderer.enabled)
        {
            m_renderer.materials[materialIndex].SetTextureOffset(textureName, uvOffset);
        }
    }
}
