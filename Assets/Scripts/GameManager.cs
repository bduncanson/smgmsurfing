﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Inst;

    [Tooltip("How long a run/wave will last (in seconds).")]
    public float m_runLength = 30.0f;

    private bool runActive = false;
    private float runTimer = 0.0f;

    [Header("Multiplier")]
    private int currentMultiplier = 0;


    private void Awake() {
        Inst = this;
        bestScore = PlayerPrefs.GetFloat("score.best", 0f);
    }

    void Update () {
	    if (runActive) {
            runTimer += Time.deltaTime;

            AddScore(Time.deltaTime);

            if (runTimer >= m_runLength) {
                EndRun();
            }
        }
	}

    public void EndRun()
    {
        runActive = false;
        Player.Inst.Reset("Run Ended");
        // crash the player
    }

    public void StartRun() {
        runTimer = 0.0f;
        runActive = true;

        lastScore = currentScore;
        currentScore = 0f;
        currentMultiplier = 1;
    }

    public float TimeLeft() {
        return Mathf.Clamp(m_runLength - runTimer, 0f, m_runLength);
    }

    public bool IsRunActive() {
        return runActive;
    }


    // SCORING
    private float currentScore = 0f;
    private float lastScore = -1f;
    private float bestScore = 0f;

    public float CurrentScore() {
        return currentScore;
    }

    public float LastScore() {
        return lastScore;
    }

    public float BestScore() {
        return bestScore;
    }

    public void AddScore(float score = 1f)
    {
        //Player.Inst.AddPowerVal(score);

        currentScore += score * CurrentMultiplier();

        if (currentScore > bestScore) {
            bestScore = currentScore;
            PlayerPrefs.SetFloat("score.best", bestScore);
        }
    }

    public int CurrentMultiplier() {

        return 1 + (int)(Player.Inst.CurrentPowerValPercent() * 4f);
    }
}
