﻿using Battlehub.SplineEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player Inst;

    private Transform _transform;

    private float boardAngle = 0.0f;                // angle of the board, positive travelling up wave, negative travelling down wave
    private float xSpeed = 10.0f;                   // a generic 'speed' we are travelling at, the faster we are moving the higher we can 'jump'

    private float bCurvePosition = 0.5f;            // where we are on the bezier curve

    [Header("Curve Points")]
    public float bCurveNeutralVal = 0.25f;         // what % of curve is the neutral/mid position
    public float bCurveShallowsVal = 0.10f;        // flat water position, will slow player
    public float bCurveOutOfBoundsVal = 0.10f;     // if player goes too far into shallow water = fail state
    public float bCurveAirbornVal = 0.656f;        // when player is airborn

    private bool wasAirbornLastFrame = false;       // temp checking to see if we were airborn in the last frame
    private bool airborn = false;

    [Header("References")]
    public GameObject m_obstacles;
    public GameObject m_pipe;
    public Transform m_rotationHolder;
    public Transform m_boardTransform;
    public ParticleSystem m_trailParticleSys;
    public LayerMask m_surfaceNormalMask;           //  only raycast the wave
    public UILandAngleIndicator m_landingIndicator;

    [Header("Settings")]
    [HideInInspector]
    public float m_maxSpeed = 20.0f;
    public float vertMultiplier = 0.72f;
    [HideInInspector]
    public float rotationModifier = 200.0f;
    public float rotationSpeed = 1.25f;   // default 1.25f
    public float m_maxHoldTimeInputLag = 0.8f; // how long we're holding input before the player reaches full rotation speed
    [HideInInspector]
    public bool freeReturnRotation = true; // not holidng any keys, allow player to keep rotating
    public bool linkForwardSpeedToPlayerAngle = false;
    public bool smoothSurfaceNormal = true;

    [Tooltip("If we are within this angle +- keep max speed")]
    public float m_maintainSpeedAngle = 30.0f;

    public float targetSpeedLerpMulti = 6.25f;
    public float slowSpeedLerpMulti = 2.5f;
    public float targetSpeedLerpMultiDecelerate = 2.5f;

    [Header("Player Speed")]
    public float m_playerMaxSpeed = 30.0f;
    public float m_playerSpeedChangeMulti = 1.0f;
    public float m_playerMinSpeed = 20.0f;
    public float m_playerStartSpeed = 25.0f;
    [HideInInspector] public float m_pSpeedVertContribution = 50f;
    [HideInInspector] public float m_mSpeedMulti = 3.0f;
    private float _playerSpeed = 0.0f;

    [Header("Dig")]
    public float m_digAngleSmoothing = 2.0f;
    private Vector3 targetDigAngle = Vector3.zero;

    // Momentum
    private float _momentum = 0.0f;             // current momentum

    public float m_initCurvePosition = 0.8f;
    public float m_initBoardAngle = -45.0f;

    [Header("Powerup")]
    public float m_maxPowerVal = 100.0f;
    public float powerupDegradeSpeed = 5.0f;
    public float maxAngleRange = 30f;           // how far from perfect before its a failure

    [Tooltip("How what % of power bar fills up when hitting perfect landing")]
    [Range(0f, 1f)] public float m_perfectPowerAdds = 0.1f;
    [Range(0f, 1f)] public float m_perfectRange = 0.9f;
    [Range(0f, 1f)] public float m_goodRange = 0.6f;
    [Range(0f, 1f)] public float m_okRange = 0.3f;

    private float _currentPowerVal = 0.0f;

    [Header("SlowMo")]
    public bool m_forceSlowMoOnAirborn = false;
    public bool m_autoRotateOnSlowMoJump = false;
    public float m_slowMoTimeScale = 0.35f;
    public float m_slowMoGravity = -0.008f;

    //public float m_slowMoLerpMulti = 1.5f;

    [Header("Forward Speed Slowdown")]
    public float m_noInputTimeToSlowdown = 2.0f; // if we're in slowdown range for this time, start to slow down
    public float m_forwardSlowSpeed = 10.0f;
    private float _noInputTimeToSlowdownTimer = 0.0f;

    [Header("Debug")]
    public float m_initialSpeed = 10.0f;
    private bool droppingIn = true;
    private bool useSwipe = false;
    public Transform m_waveHeightPosition;
    public float m_minBackwardSpeed = 12.0f;
    public float m_minForwardSpeed = 16.0f;

    [Header("Spline")]
    public SplineBase m_spline;

    [Header("Vert Force")]
    public float onWaveGravity = 5.0f;
    public float _initGravForce = -1.0f;
    public float _gravFalloffMulti = 1.0f;
    public float m_pushMultiDown = 0.005f;
    public float m_pushMultiUp = 0.001f;
    private float _gravForce = 0.0f;
    private float _lastVertForce = 0.0f;

    private float bANormalised = 0f;



    private void Awake() {
        Inst = this;
        _transform = GetComponent<Transform>();
    }

    private void Start() {
        bCurvePosition = bCurveNeutralVal;
        xSpeed = m_initialSpeed;

        boardAngle = m_initBoardAngle;
        bCurvePosition = m_initCurvePosition;
        airborn = true;


        Application.targetFrameRate = 60;

        m_spline.GetPoint(0.5f);

        #if UNITY_STANDALONE
        Screen.SetResolution(640, 1136, false);
        Screen.fullScreen = true;
        #endif
    }

    private bool useVertForce = true;

    private void Update()
    {
        if (!GameManager.Inst.IsRunActive()) {

            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
                GameManager.Inst.StartRun();

            _playerSpeed = m_playerStartSpeed;

            _transform.position = m_spline.GetPoint(bCurvePosition);
            m_rotationHolder.localEulerAngles = new Vector3(m_rotationHolder.localEulerAngles.x, boardAngle, m_rotationHolder.localEulerAngles.z);

            return;
        }

        if (m_waveHeightPosition)
            m_waveHeightPosition.position = m_spline.GetPoint(bCurveAirbornVal);

        bANormalised = BoardAngleNormalised();

        //if (droppingIn && bCurvePosition < bCurveAirbornVal)
        //    droppingIn = false;

        if (!droppingIn)
        {
            if (useSwipe)
                HandleInput_Drift();
            else {
                HandleInput_TwoButton();
                //HandleInput_OneButton();
            }
        }

        // Rotation Dig into water
        if (!airborn)
            m_rotationHolder.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(m_rotationHolder.transform.localEulerAngles), Quaternion.Euler(targetDigAngle), Time.deltaTime * m_digAngleSmoothing);
        else // reset if in the air
            m_rotationHolder.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(m_rotationHolder.transform.localEulerAngles), Quaternion.Euler(new Vector3(0f, m_rotationHolder.localEulerAngles.y, m_rotationHolder.localEulerAngles.z)), Time.deltaTime * m_digAngleSmoothing);

        if (airborn)
            m_landingIndicator.SetCurrentAngle(BoardAngleNormalised());

        // DEBUG - stop vertical force
        if (Input.GetKeyDown(KeyCode.C))
            useVertForce = !useVertForce;

        // Adjust vert position
        if (useVertForce)
            bCurvePosition += VertForce();

        // Player Speed
        PlayerSpeedCalculation();

        // Adjust 'forward' speeds
        ForwardSpeedCalculation();

        // keep values within 0 -> 1 for curve position
        bCurvePosition = Mathf.Clamp(bCurvePosition, 0.0f, 1.0f);

        // Degrade powerup
        if (_currentPowerVal > 0f)
            _currentPowerVal = Mathf.Clamp(_currentPowerVal - Time.deltaTime * powerupDegradeSpeed, 0f, m_maxPowerVal);

        //gravity drag on wave
        if (!airborn && BoardAngleNormalised() < 20.0f && BoardAngleNormalised() > -20.0f && bCurvePosition > bCurveShallowsVal)
            bCurvePosition -= Time.deltaTime * onWaveGravity;

        // place on curve at correct position
        //_transform.position = m_curve.GetPointAt(bCurvePosition);
        _transform.position = m_spline.GetPoint(bCurvePosition);

        if (airborn && doSlowMoJump)
        {
            //if (-jumpAngle < -90f)
            //    boardAngle = Mathf.Clamp(boardAngle - rotationSpeed * (Time.deltaTime * (1 / Time.timeScale)) * (200f * (_holdTime / m_maxHoldTimeInputLag)), ;

            //boardAngle = Mathf.Lerp(boardAngle, -jumpAngle, 
        }

        // apply rotation
        m_rotationHolder.localEulerAngles = new Vector3(m_rotationHolder.localEulerAngles.x, boardAngle, m_rotationHolder.localEulerAngles.z);

        // Find normal of water surface   
        RaycastHit hit;
        Vector3 castPos = new Vector3(_transform.position.x, _transform.position.y - .05f, _transform.position.z);
        if (Physics.Raycast(castPos, -_transform.up, out hit, 5.85f, m_surfaceNormalMask))
        {
            if (smoothSurfaceNormal) {
                Vector3 upWeighted = Mathf.Clamp(1 - (Mathf.Abs(BoardAngleNormalised()) / 90f), 0f, 1f) * Vector3.up + hit.normal;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.FromToRotation(Vector3.up, upWeighted), 200.0f * Time.deltaTime);
            }
            else {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.FromToRotation(Vector3.up, hit.normal), 200.0f * Time.deltaTime);
            }
        }

        // fail if landing on wrong angle
        if (wasAirbornLastFrame && bCurvePosition < bCurveAirbornVal) {
            if (BoardAngleNormalised() > 0.0f || BoardAngleNormalised() < -215.0f)
            {
                UILandMessageManager.Inst.NewMessage("GNARLY LANDING", Color.red, 2.0f);
                OnPlayerDeath("landing wrong angle");
            }

        }

        // fail if going out of bounds, or slowing down too much
        if (bCurvePosition < bCurveOutOfBoundsVal)
            OnPlayerDeath("out of bounds (" + bCurvePosition + ")");
    }

    private void LateUpdate()
    {
        if (bCurvePosition > bCurveAirbornVal)
            wasAirbornLastFrame = true;
        else
            wasAirbornLastFrame = false;

        // Slowmo
        if (m_forceSlowMoOnAirborn) {
            if (airborn) {
                float tScale = Mathf.Lerp(1.0f, m_slowMoTimeScale, (bCurvePosition - bCurveAirbornVal) / 0.1f);
                Time.timeScale = tScale;
            }
        }
        else
        {
            if (airborn && doSlowMoJump)
            {
                float tScale = Mathf.Lerp(1.0f, m_slowMoTimeScale, (bCurvePosition - bCurveAirbornVal) / 0.1f);
                Time.timeScale = tScale;
            }
        }
    }

    private bool doSlowMoJump = false;

    // setup jump angle ui indicator
    void SetJumpIndicatorActive(bool active, float jumpAngle = -1f, float landAngle = -1f)
    {
        m_landingIndicator.SetIndicatorActive(active);

        m_landingIndicator.SetCurrentAngle(jumpAngle);
        m_landingIndicator.SetLandAngle(landAngle);
    }

    void OnAirborn()
    {
        airborn = true;

        // setup a slowmo jump
        // disabled for now <><><><><>
        if (CurrentPowerValPercent() > 0.5f) {
            //doSlowMoJump = true;
            //_gravForce = m_slowMoGravity;
        }

        // Show the UI angle indicator for the jump
        SetJumpIndicatorActive(true, jumpAngle, -jumpAngle);

        // disable particles
        var em = m_trailParticleSys.emission;
        em.enabled = false;
    }

    void OnLand(float angle) {

        airborn = false;

        SetJumpIndicatorActive(false);

        if (doSlowMoJump)
        {
            //AddPowerVal(-(m_maxPowerVal * 0.5f));     // reduce power value on successful slow-mo landing
            doSlowMoJump = false;
        }

        // Calculate landing angle/power
        if (!droppingIn)
        {
            // check for angle of jumping
            float jumpSuccess = LandScore(jumpAngle, angle);

            if (jumpSuccess >= m_perfectRange) {
                //Debug.LogWarning("Perfect: " + jumpSuccess);
                UILandMessageManager.Inst.NewMessage("PERFECT!", Color.green);
            }
            else if (jumpSuccess >= m_goodRange) {
                //Debug.LogWarning("Good: " + jumpSuccess);
                UILandMessageManager.Inst.NewMessage("GOOD!", Color.blue);
            }
            else if (jumpSuccess >= m_okRange) {
                //Debug.LogWarning("Ok: " + jumpSuccess);
                UILandMessageManager.Inst.NewMessage("OK!", Color.yellow);
            }

            GameManager.Inst.AddScore(jumpSuccess);
            AddPowerVal(jumpSuccess * (m_perfectPowerAdds * m_maxPowerVal));

            // add some player speed when landing good jumps
            //_playerSpeed += m_playerMaxSpeed * (0.05f * jumpSuccess);
        }

        // if we were dropping in, set as not!
        if (droppingIn)
            droppingIn = false;

        // enable particles
        var em = m_trailParticleSys.emission;
        em.enabled = true;
    }

    public void AddPowerVal(float val) {
        _currentPowerVal = Mathf.Clamp(_currentPowerVal + val, 0f, m_maxPowerVal);
    }

    public void AddPowerPercent(float val) {
        _currentPowerVal = Mathf.Clamp(_currentPowerVal + (val * m_maxPowerVal), 0f, m_maxPowerVal);
    }

    // How much powerup juice do we have
    public float CurrentPowerVal() {
        return Mathf.Clamp(_currentPowerVal, 0f, m_maxPowerVal);
    }

    public float CurrentPowerValPercent() {
        return Mathf.Clamp(_currentPowerVal / m_maxPowerVal, 0f, 1f);
    }

    // Give % score how well you land 0->1f
    private float LandScore(float jumpAngle, float landAngle)
    {
        float successPercent = 0.0f;

        float rangeMin = -jumpAngle - maxAngleRange;
        float rangeMax = -jumpAngle + maxAngleRange;

        // If we're outside of the land angle range, give 0 score (probably fail at some point too)
        if (landAngle < rangeMin || landAngle > rangeMax)
            successPercent = 0.0f;
        else
        {
            // perfect!
            if (landAngle == -jumpAngle)
                successPercent = 1.0f;

            if (landAngle < -jumpAngle) {
                successPercent = 1f - (Mathf.Abs((landAngle + jumpAngle)) / maxAngleRange);
            }
            else {
                successPercent = 1f - (Mathf.Abs((landAngle + jumpAngle)) / maxAngleRange);
            }
        }

        return successPercent;
    }

    // Move player along curve based on board angle
    private float VertForce()
    {
        float f = 0.0f;

        if (bCurvePosition < bCurveAirbornVal)
        {
            if (airborn) {
                //_momentum = _momentum * 0.35f;   /// removed mometum drop on landing
                OnLand(BoardAngleNormalised());
            }

            // Simple Vertical Speed
            float angle = BoardAngleNormalised();

            if (angle >= 0.0f)
            {
                // top-right
                if (angle <= 90.0f) {
                    f = (angle / 90.0f) * VertForceWithForwardSpeed();
                }
                // top-left
                else {
                    f = (1 - ((angle - 90f) / 90.0f)) * VertForceWithForwardSpeed();
                }
            }
            else
            {
                // bot-right
                if (angle >= -90f) {
                    f = (Mathf.Abs(angle) / 90.0f) * -VertForceWithForwardSpeed();
                }
                // bot-left
                else {
                    f = (1 - ((Mathf.Abs(angle) - 90f) / 90.0f)) * -VertForceWithForwardSpeed();
                }
            }

            _gravForce = _initGravForce;
        }
        else
        {
            if (!airborn) {
                f = (BoardAnglePercent(boardAngle) * VertForceWithForwardSpeed()) + (1.5f * _momentum);
                jumpAngle = BoardAngleNormalised();
                OnAirborn();
            }

            f = _lastVertForce;
            _gravForce -= Time.deltaTime * _gravFalloffMulti;
            f += _gravForce;
        }

        _lastVertForce = f;
        return _lastVertForce * Time.deltaTime;
    }

    private float _holdTime = 0.0f;

    private float _maxReleaseTime = 1.75f;
    private float _releaseTime = 0.0f;

    [Header("Input")]
    public AnimationCurve m_inputCurve;
    public float m_inputCurveMulti = 1.0f;
    public float m_inputWidthPercent = 1.0f;    // percent of screen width to use

    public void OnSensitivityChanged(float newVal)
    {
        Debug.Log(newVal);
        m_inputCurveMulti = newVal;
    }

    public void OnScreenWidthChanged(float newVal)
    {
        Debug.Log(newVal);
        m_inputWidthPercent = newVal;
    }

    void HandleInput_Drift()
    {
        // give more rotation, the more momentum you have
        float mRot = rotationModifier;

        mRot = rotationModifier + (4.0f * rotationModifier);

        if (Input.GetMouseButton(0))
        {
            float halfWidth = Screen.width * 0.5f;
            float halfWidthAltered = (Screen.width * m_inputWidthPercent) * 0.5f;
            float mousePosX = Mathf.Clamp(Input.mousePosition.x, halfWidth - (halfWidthAltered * 0.5f), halfWidth + (halfWidthAltered * 0.5f));

            if (mousePosX < halfWidth)
            {
                float val = m_inputCurve.Evaluate(1 - ((mousePosX - halfWidthAltered) / halfWidthAltered)) * m_inputCurveMulti;
                boardAngle -= (val) * Time.deltaTime * mRot;

                // apply board 'dig' rotation
                //m_boardTransform.localEulerAngles = new Vector3((1 - (mousePosX / halfWidthAltered)) * -30, 0.0f, 0.0f);

                if (airborn)
                {
                    if (boardAngle < -180.0f)
                        boardAngle += 360.0f;
                }
            }
            else
            {
                float val = m_inputCurve.Evaluate((mousePosX - halfWidth) / halfWidthAltered) * m_inputCurveMulti;

                boardAngle += (val) * Time.deltaTime * mRot;

                // apply board 'dig' rotation
                //m_boardTransform.localEulerAngles = new Vector3((((mousePosX - (halfWidthAltered * 0.5f)) / (halfWidthAltered * 0.5f)) * 30), 0.0f, 0.0f);

                if (airborn)
                {
                    if (boardAngle > 180.0f)
                        boardAngle -= 360.0f;
                }
            }
        }
        else
            m_boardTransform.localEulerAngles = new Vector3(0f, 0.0f, 0.0f);

        // normalise board angles
        if (!airborn)
        {
            if (boardAngle < -180.0f)
            {
                boardAngle += 360.0f;
                _momentum = 0.0f;
            }
            else if (boardAngle > 180.0f)
            {
                boardAngle -= 360.0f;
                _momentum = 0.0f;
            }
        }
    }

    void HandleInput_TwoButton()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || (Input.GetMouseButton(0) && Input.mousePosition.x < Screen.width * 0.5f)) {

            // auto rotate on slowmo jump
            if (!m_autoRotateOnSlowMoJump || (!airborn && !doSlowMoJump))
                boardAngle -= rotationSpeed * (Time.deltaTime * (1 / Time.timeScale)) * (200f * (_holdTime / m_maxHoldTimeInputLag));

            // add smoothing to initial turn speed
            _holdTime = Mathf.Clamp(_holdTime + Time.deltaTime, 0f, m_maxHoldTimeInputLag);

            // pivot testing
            if (BoardAngleNormalised() < 0f)
                targetDigAngle = new Vector3(-45f, m_rotationHolder.localEulerAngles.y, 0f);
            else
                targetDigAngle = new Vector3(-10f, m_rotationHolder.localEulerAngles.y, 0f);
        }
        else if (Input.GetKey(KeyCode.RightArrow) || (Input.GetMouseButton(0) && Input.mousePosition.x >= Screen.width * 0.5f)) {

            // auto rotate on slowmo jump
            if (!m_autoRotateOnSlowMoJump || (!airborn && !doSlowMoJump))
                boardAngle += rotationSpeed * (Time.deltaTime * (1 / Time.timeScale)) * (200f * (_holdTime / m_maxHoldTimeInputLag));

            // add smoothing to initial turn speed
            _holdTime = Mathf.Clamp(_holdTime + Time.deltaTime, 0f, m_maxHoldTimeInputLag);

            // pivot testing
            if (BoardAngleNormalised() > 0f)
                targetDigAngle = new Vector3(45f, m_rotationHolder.localEulerAngles.y, 0f);
            else
                targetDigAngle = new Vector3(10f, m_rotationHolder.localEulerAngles.y, 0f);
        }

        if (Input.GetMouseButtonDown(0)) {
            _noInputTimeToSlowdownTimer = 0.0f;
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetMouseButtonUp(0))
        {
            _holdTime = 0.0f;

            // pivot testing
            targetDigAngle = new Vector3(0f, m_rotationHolder.localEulerAngles.y, m_rotationHolder.localEulerAngles.z);
        }

        //if (airborn)
        //    targetDigAngle = new Vector3(0f, m_rotationHolder.localEulerAngles.y, m_rotationHolder.localEulerAngles.z);

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        // normalise board angles
        //if (!airborn) {
        if (boardAngle < -180.0f) {
            boardAngle += 360.0f;
            _momentum = 0.0f;
        }
        else if (boardAngle > 180.0f) {
            boardAngle -= 360.0f;
            _momentum = 0.0f;
        }
        //}

    }

    void HandleInput_OneButton()
    {
        float mRot = rotationModifier;

        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
        {
            _releaseTime = 0.0f;

            // blocking backwards turns when trying to go for a jump near the top of a wave
            if ((bCurvePosition >= bCurveAirbornVal || bCurvePosition < bCurveAirbornVal - 0.025f) ||
                (bCurvePosition >= bCurveAirbornVal - 0.025f && bCurvePosition < bCurveAirbornVal && BoardAngleNormalised() < 90.0f))
            {
                _holdTime = Mathf.Clamp(_holdTime + Time.deltaTime, 0f, m_maxHoldTimeInputLag);

                boardAngle -= 2 * Time.deltaTime * mRot + (_holdTime / m_maxHoldTimeInputLag) * 0.85f;

                if (boardAngle > 180.0f)
                    boardAngle -= 360.0f;
            }
        }
        else
        {
            if (freeReturnRotation)
                boardAngle += rotationSpeed * Time.deltaTime * mRot + (_releaseTime / _maxReleaseTime) * 1.25f;
            else
            {
                if (BoardAngleNormalised() > -45f || BoardAngleNormalised() < -90f) {
                    boardAngle += rotationSpeed * Time.deltaTime * mRot + (_releaseTime / _maxReleaseTime) * 1.25f;
                }
            }

            _releaseTime = Mathf.Clamp(_releaseTime + Time.deltaTime, 0f, _maxReleaseTime);

            if (boardAngle < -180.0f)
                boardAngle += 360.0f;
        }

        if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0))
            _holdTime = 0.0f;

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        // normalise board angles
        if (!airborn)
        {
            if (boardAngle < -180.0f)
            {
                boardAngle += 360.0f;
                _momentum = 0.0f;
            }
            else if (boardAngle > 180.0f)
            {
                boardAngle -= 360.0f;
                _momentum = 0.0f;
            }
        }
    }

    private float _targetSpeed = 20.0f;
    private float jumpAngle = 0.0f;

    // Calculate forward speed changes
    private void ForwardSpeedCalculation()
    {
        //float targetSpeed = m_maxSpeed;

        if (!airborn)
        {
            // NEW AND IMPROVED
            // 0 - 90 : 90 = reduce speed to standstil / 0 = build speed

            float bAngle = BoardAngleNormalised();

            float remainAngle = 90f - m_maintainSpeedAngle; // deceleration zone

            if (linkForwardSpeedToPlayerAngle)
            {
                // Slow Zone
                if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow) && BoardAngleNormalised() > -90f && BoardAngleNormalised() < 90f)
                    _noInputTimeToSlowdownTimer += Time.deltaTime;
                else
                    _noInputTimeToSlowdownTimer = 0.0f;

                // Are we trying to slow down and ride inside the pipe?
                if (RidingThePipe()) {
                    //_targetSpeed = m_forwardSlowSpeed;
                    _targetSpeed = PlayerMaxSpeed() * m_mSpeedMulti;
                }
                else
                {
                    // top-right quad
                    if (BoardAngleNormalised() >= 0f && BoardAngleNormalised() <= 90.0f)
                    {
                        if (bAngle <= m_maintainSpeedAngle) {
                            _targetSpeed = m_maxSpeed;
                        }
                        else {
                            _targetSpeed = (1 - ((Mathf.Abs(BoardAngleNormalised()) - m_maintainSpeedAngle) / remainAngle)) * m_maxSpeed;
                        }
                    }
                    // bot-right quad
                    else if (BoardAngleNormalised() >= -90f && BoardAngleNormalised() < 0f)
                    {
                        if (bAngle >= -m_maintainSpeedAngle) {
                            _targetSpeed = m_maxSpeed;
                        }
                        else {
                            _targetSpeed = (1 - ((Mathf.Abs(BoardAngleNormalised()) - m_maintainSpeedAngle) / remainAngle)) * m_maxSpeed;
                        }
                    }
                    // bot-left quad
                    else if (BoardAngleNormalised() < -90f) {
                        _targetSpeed = ((Mathf.Abs(BoardAngleNormalised()) - 90f) / 90.0f) * -m_maxSpeed;
                    }
                    // top-left quad
                    else if (BoardAngleNormalised() > 90f) {
                        _targetSpeed = ((BoardAngleNormalised() - 90f) / 90.0f) * -m_maxSpeed;
                    }
                }
            }
            else
            {
                float reverseSpeedMulti = 50f;
                float forwardSlowMulti = 15.0f; // default 5.0f

                if (BoardAngleNormalised() > 0.0f && BoardAngleNormalised() <= 90.0f)
                {
                    if (_targetSpeed < 0.0f)
                        _targetSpeed += (BoardAngleNormalised() / 90.0f) * Time.deltaTime * 5.0f; // was 500
                    else
                        _targetSpeed = Mathf.Clamp(_targetSpeed - (BoardAngleNormalised() / 90.0f) * Time.deltaTime * forwardSlowMulti, -m_maxSpeed, m_maxSpeed); // was 500
                }
                else if (BoardAngleNormalised() > 90.0f && BoardAngleNormalised() <= 180.0f)
                {
                    if (_targetSpeed >= 0.0f)
                        _targetSpeed -= (BoardAngleNormalised() / 180.0f) * Time.deltaTime * reverseSpeedMulti; // was 500
                    else
                        _targetSpeed += (BoardAngleNormalised() / 180.0f) * Time.deltaTime * 50.0f; // was 500
                }
                else if (BoardAngleNormalised() < 0.0f && BoardAngleNormalised() >= -90.0f)
                {
                    // slow down forward
                    if (BoardAngleNormalised() < -75.0f) {
                        _targetSpeed = Mathf.Clamp(_targetSpeed - (Mathf.Abs(BoardAngleNormalised()) / 90.0f) * Time.deltaTime * 90.0f, 0f, m_maxSpeed); // was 500
                    }
                    else {
                        _targetSpeed += (1 - (BoardAngleNormalised() / 90.0f)) * Time.deltaTime * 90.0f; // was 500
                    }
                }
                else {
                    _targetSpeed += (BoardAngleNormalised() / 180.0f) * Time.deltaTime * 90.0f; // was 500
                }
            }
        }
        else
        {
            if (!droppingIn) {
                //_targetSpeed -= (jumpAngle / 180f) * Time.deltaTime * 5.0f;

                if (_targetSpeed > 0.0f)
                    _targetSpeed -= (jumpAngle / 180f) * Time.deltaTime * 10.0f; // was 5
                else
                    _targetSpeed += (jumpAngle / 180f) * Time.deltaTime * 25.0f; // was 15

            }
            else {
                _targetSpeed = m_maxSpeed;
            }
        }

        // if vertical, forward speed = 0, wave catches up
        // if horizontal backwards, forward speed is -max, wave catches up

        _targetSpeed = Mathf.Clamp(_targetSpeed, -m_maxSpeed, m_maxSpeed);

        if (linkForwardSpeedToPlayerAngle)
        {
            //if (RidingThePipe()) {
            //    xSpeed = Mathf.Lerp(xSpeed, _targetSpeed, Time.deltaTime * slowSpeedLerpMulti);
            //}
            //else
            //{
                if (_targetSpeed < xSpeed) {
                    xSpeed = Mathf.Lerp(xSpeed, _targetSpeed, Time.deltaTime * targetSpeedLerpMultiDecelerate);
                }
                else {
                    xSpeed = Mathf.Lerp(xSpeed, _targetSpeed, Time.deltaTime * targetSpeedLerpMulti);
                }
            //}
        }
        else
            xSpeed = Mathf.Lerp(xSpeed, _targetSpeed, Time.deltaTime * 6.25f);
    }


    private void PlayerSpeedCalculation()
    {   
        float bAngle = BoardAngleNormalised();

        // if we're going down the wave, build up speed
        if (bAngle < 0f) {
            if (bAngle >= -90f) {
                _playerSpeed += ((Mathf.Abs(bAngle) / 90f) * m_playerSpeedChangeMulti * Time.deltaTime) * 1.2f;
            }
            else {
                _playerSpeed += ((1 - ((Mathf.Abs(bAngle) - 90f) / 90f)) * m_playerSpeedChangeMulti * Time.deltaTime) * 1.2f;
            }
        }

        // if we're going up the wave, bleed off speed
        else if (bAngle > 0f)
        {
            if (bAngle <= 90f) {
                _playerSpeed -= (bAngle / 90f) * m_playerSpeedChangeMulti * Time.deltaTime;
            }
            else {
                _playerSpeed -= (1 - ((bAngle - 90f) / 90f)) * m_playerSpeedChangeMulti * Time.deltaTime;
            }
        }

        _playerSpeed = Mathf.Clamp(_playerSpeed, PlayerMinSpeed(), PlayerMaxSpeed());

        //Debug.Log(_playerSpeed);

        m_maxSpeed = _playerSpeed * m_mSpeedMulti;
    }

    private float VertForceWithForwardSpeed() {
        return vertMultiplier + (m_pSpeedVertContribution * _playerSpeed) * Time.deltaTime;
    }

    private float PlayerMinSpeed() {
        return m_playerMinSpeed;
    }

    private float PlayerMaxSpeed()
    {
        if (RidingThePipe())
            return m_forwardSlowSpeed;
        else
            return m_playerMaxSpeed;
    }

    // Return how quicly we should change forward speed based on angle of board
    // angle of 90 = max reduction
    // angle of 0 = min reduction
    // angle of -90 = max addition
    private float ForwardAccelerationModifier(float angle)
    {
        angle = Mathf.Clamp(angle, -90.0f, 90.0f);

        if (angle > 0f)
            return (angle / 90.0f) * 0.75f;   // we're slowing down

        else if (angle < 0f)    // we're speeding up
            return (Mathf.Abs(angle) / 90.0f) * 1.2f;

        return 0.10f;   // 10% speed reduction if we're traveling horizontal
    }

    private float BoardAnglePercent(float angle)
    {
        if (angle > 90.0f)
            angle = 90.0f - (angle - 90.0f);
        else if (angle < -90.0f)
            angle = -90.0f - (angle + 90.0f);

        //todo: need to factor for angles other than 0->90 now
        angle = Mathf.Clamp(angle, -90.0f, 90.0f);
        return angle / 90.0f;
    }

    public void OnPlayerDeath(string reason = "")
    {
        GameManager.Inst.EndRun();
    }

    public void Reset(string reason = "")
    {
        if (reason != "") Debug.LogWarning(reason);


        _currentPowerVal = 0.0f;
        boardAngle = 0.0f;
        xSpeed = m_initialSpeed;
        bCurvePosition = bCurveNeutralVal;
        _momentum = 0.0f;

        boardAngle = m_initBoardAngle;
        bCurvePosition = m_initCurvePosition;
        airborn = true;
        droppingIn = true;
        _lastVertForce = 0.0f;
        _gravForce = 0.0f;

        _transform.eulerAngles = new Vector3(-90.0f, 0.0f, 0.0f);

        if (m_pipe.activeSelf)
            m_pipe.BroadcastMessage("OnReset");

        if (m_obstacles.activeSelf)
            m_obstacles.BroadcastMessage("OnReset");
    }

    // Are we trying to slow down and ride inside the pipe?
    private bool RidingThePipe() {
        if (_noInputTimeToSlowdownTimer > m_noInputTimeToSlowdown)
            return true;

        return false;
    }

    // Current 'forward' speed of the player
    public float PlayerSpeed() {
        return xSpeed;
    }

    public float ActualPlayerSpeed() {
        return _playerSpeed;
    }

    public int PlayerDirection()
    {
        float bAngleNorm = BoardAngleNormalised();

        if (bAngleNorm >= -90f && bAngleNorm <= 90f)
            return 1;
        else
            return -1;
    }

    //public float PlayerSpeed_X()
    //{
    //    float angle = Mathf.Abs(BoardAngleNormalised());
    //    float speed = 0f;
    //    if (angle <= 90f)
    //        speed = _playerSpeed * (1 - (angle / 90f));
    //    else
    //        speed = -_playerSpeed * ((angle - 90f) / 90f);

    //    Debug.Log(speed);
    //    return speed;
    //}

    public float PlayerSpeedPercentage()
    {
        float angle = 0;

        if (!airborn) {
            angle = Mathf.Abs(BoardAngleNormalised());
        }
        else {
            angle = jumpAngle;
        }

        float speed = 0f;
        if (angle <= 90f)
            speed = (1 - (angle / 90f));
        else
            speed = -((angle - 90f) / 90f);

        return speed;
    }

    public float SpeedPercentage() {
        return Mathf.Abs(xSpeed) / m_maxSpeed;
    }

    public float SpeedPercentageFull()
    {
        return (m_maxSpeed + xSpeed) / (m_maxSpeed * 2f);

    }

    public float Momentum() {
        return _momentum;
    }

  

    private float AngleNormalised(float angle)
    {
        if (angle > 0f) {
            return 180.0f - angle;
        }
        else {
            return -180.0f + Mathf.Abs(angle);
        }
    }

    public float BoardAngleNormalised()
    {
        if (boardAngle > 0f) {
            return 180.0f - boardAngle;
        }
        else {
            return -180.0f + Mathf.Abs(boardAngle);
        }
    }

    public void OnClick_SwapInputs() {
        useSwipe = !useSwipe;
    }

    public bool UsingSwipeControls() {
        return useSwipe;
    }
}