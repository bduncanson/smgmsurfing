﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {

    public Player m_player;
    public float m_speedMultiplier = 0.5f;
    public float m_resetPos = 30.0f;
    public Vector3 m_spawnPos;

    private Vector3 startPos;

    public float m_zMovement = 0.0f;

	void Start () {
        startPos = transform.position;

        if (m_player == null)
            m_player = Player.Inst;
    }
	
	void Update () {
        if (!GameManager.Inst.IsRunActive())
            return;

        transform.localPosition -= new Vector3(1.0f, 0.0f, m_zMovement) * Time.deltaTime * m_player.PlayerSpeed() * m_speedMultiplier;

        if (transform.localPosition.x < m_resetPos)
        {
            transform.position = startPos;

            // just reset coins if we reach the end 
            gameObject.BroadcastMessage("OnPickupReset", SendMessageOptions.DontRequireReceiver);

            Pickup p = GetComponent<Pickup>();
            if (p != null) p.OnReset();
        }
    }

    public void OnReset() {
        transform.position = startPos;
    }
}
