﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    [Header("References")]
    public Transform m_movementHolder;
    public Transform m_followTarget;
    public Player m_player;
    private Camera _camera;

    private Transform _transform;
    private Vector3 _defaultPosition;   // Starting position for camera to reset to
    private Vector3 _basePosition = Vector3.zero; // where we should be without any noise applied

    public float m_moveHeightTrigger = 5.8f;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _transform = GetComponent<Transform>();
        _defaultPosition = m_movementHolder.position;
        _basePosition = m_movementHolder.position;
    }

    private void LateUpdate()
    {
        if (m_followTarget == null || m_player == null)
            return;
        if (m_player.transform.position.y >= m_moveHeightTrigger)
        {
            //_transform.position = new Vector3(_transform.position.x, _defaultPosition.y + m_followTarget.transform.position.y - m_moveHeightTrigger, _transform.position.z);
            _basePosition = new Vector3(_basePosition.x, _defaultPosition.y + m_followTarget.transform.position.y - m_moveHeightTrigger, _basePosition.z); 
        }

        SteadycamNoiseMotion();
        SteadycamZoom();

        // FOV shifting
        float speedPercent = Mathf.Lerp(-10f, 10f, m_player.SpeedPercentage());
        _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, m_defaultFov + fovNoise + speedPercent, Time.deltaTime * m_fovNoiseSmoothing);

        // steadycam offset
        float speedPunchVal = Mathf.Lerp(-8f, 1f, m_player.SpeedPercentageFull());
        Vector3 speedPunch = Vector3.zero;
        speedPunch.x = speedPunchVal;
        m_movementHolder.localPosition = Vector3.Lerp(m_movementHolder.position, _basePosition + noiseOffset + speedPunch, Time.deltaTime * m_cameraSmoothing);

        // rotation
        // -26 backwards -> 0 forwards

        //float rotation = Mathf.Lerp(-26f, 0f, m_player.SpeedPercentageFull());
        //m_movementHolder.eulerAngles = new Vector3(0f, rotation, 0f);
    }

    [Header("Settings")]
    public float m_cameraSmoothing = 2.0f;

    [Header("Camera Shake")]
    public float m_xNoiseVal = 1.0f;
    public float m_yNoiseVal = 1.0f;
    public float m_zNoiseVal = 1.0f;
    Vector3 noiseOffset = Vector3.zero;

    [Header("Camera Zoom")]
    public float m_fovNoiseSmoothing = 2.0f;
    public float m_fovNoiseVal = 1.0f;
    public float m_defaultFov = 75.0f;
    private float fovNoise = 0.0f; 

    void SteadycamNoiseMotion() {
        noiseOffset.x = m_xNoiseVal * Mathf.PerlinNoise(Time.time, 0.0f);
        noiseOffset.y = m_yNoiseVal * Mathf.PerlinNoise(Time.time, 0.0f);
        noiseOffset.z = m_zNoiseVal * Mathf.PerlinNoise(Time.time, 0.0f);
    }

    void SteadycamZoom() {
        fovNoise = m_fovNoiseVal * Mathf.PerlinNoise(Time.time, 0.0f);
    }
}