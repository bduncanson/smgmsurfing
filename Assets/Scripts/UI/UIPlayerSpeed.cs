﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerSpeed : MonoBehaviour {

    public Player m_player;
    public Text m_textSpeed;
    public Text m_textMultiplier;
    public Text m_textScore;
    public Text m_textMomentum;
    public Text m_txtLastScore;
    public Text m_txtBestScore;
    public Text m_boardAngleNormalised;

    public Text m_powerValText;
    public Image m_powerValImg;
    private float _powerVal = -1f;
    private float _powerValPercent = -1f;

    private float _lastSpeed = 0.0f;
    private float _lastBest = -1;
    private int _lastMulti = -1;


	void Update () {
        if (m_player.PlayerSpeed() != _lastSpeed)
        {
            _lastSpeed = m_player.PlayerSpeed();
            m_textSpeed.text = "speed: " + _lastSpeed.ToString("N1");
        }

        if (_lastMulti != GameManager.Inst.CurrentMultiplier())
        {
            _lastMulti = GameManager.Inst.CurrentMultiplier();
            m_textMultiplier.text = "x" + _lastMulti.ToString();
        }

        if (GameManager.Inst.LastScore() >= 0)
            m_txtLastScore.text = "Last: " + GameManager.Inst.LastScore().ToString("N1");
        else
            m_txtLastScore.text = "Last: -";

        if (_lastSpeed != GameManager.Inst.BestScore())
        {
            _lastBest = GameManager.Inst.BestScore();
            m_txtBestScore.text = "Best: " + _lastBest.ToString("N1");
        }

        m_textScore.text = GameManager.Inst.CurrentScore().ToString("N0");
        m_textMomentum.text = "momentum: " + (m_player.Momentum() * 1000.0f).ToString("N1");

        m_boardAngleNormalised.text = m_player.BoardAngleNormalised().ToString("N0");


        if (m_player.CurrentPowerVal() != _powerVal)
        {
            _powerVal = m_player.CurrentPowerVal();
            m_powerValText.text = m_player.CurrentPowerVal().ToString("N0");
            _powerValPercent = m_player.CurrentPowerValPercent();
        }

        if (m_powerValImg.fillAmount != _powerValPercent) {
            m_powerValImg.fillAmount = Mathf.Lerp(m_powerValImg.fillAmount, _powerValPercent, Time.deltaTime * 3.0f); 
        }
    }
}
