﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILandAngleIndicator : MonoBehaviour {

    public GameObject m_indicatorObj;

    public RectTransform m_jumpAngle;
    public RectTransform m_currentAngle;


    public void SetIndicatorActive(bool active)
    {
        m_indicatorObj.SetActive(active);
    }

    public void SetCurrentAngle(float angle)
    {
        m_currentAngle.eulerAngles = new Vector3(0f, 0f, angle);
    }

    public void SetLandAngle(float angle)
    {
        m_jumpAngle.eulerAngles = new Vector3(0f, 0f, angle);
    }
}
