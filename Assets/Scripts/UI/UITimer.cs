﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour {

    public Text m_countdownText;

	void Update () {
        m_countdownText.text = GameManager.Inst.TimeLeft().ToString("N1");
	}
}
