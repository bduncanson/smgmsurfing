﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILandMessageManager : MonoBehaviour {

    public static UILandMessageManager Inst;

    public GameObject m_messagePrefab;

    public float m_defaultTimeOnScreen = 1.0f;

    private void Awake() {
        Inst = this;
    }

    public void NewMessage(string msg, Color c, float timeOnScreen = -1f)
    {
        if (m_messagePrefab == null)
            return;

        if (timeOnScreen <= 0.0f)
            timeOnScreen = m_defaultTimeOnScreen;

        GameObject g = Instantiate(m_messagePrefab);

        g.transform.SetParent(transform);
        g.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -100f);
        g.GetComponent<UIMessage>().Setup(msg, timeOnScreen, c);
    }
}
