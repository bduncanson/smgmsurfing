﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISwapInputs : MonoBehaviour {

    public Player m_player;
    public Text m_text;

    private void Start()
    {
        if (m_player.UsingSwipeControls())
            m_text.text = "CURRENT: SWIPE";
        else
            m_text.text = "CURRENT: 1-BTN";
    }

    public void OnClick()
    {
        m_player.OnClick_SwapInputs();
        Start();
    }
}
