﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMessage : MonoBehaviour {

    public Text m_text;
    public CanvasGroup m_canvasGroup;

    private bool setup = false;
    private float fadeTime;
    private float fadeTimer = 0.0f;

    private void Update()
    {
        if (setup)
        {
            fadeTimer += Time.deltaTime;
            m_canvasGroup.alpha = Mathf.Lerp(0f, 1f, 1 - Mathf.Clamp(fadeTimer / fadeTime, 0f, 1f));

            if (fadeTimer > fadeTime)
                Destroy(gameObject);
        }
    }

    public void Setup(string msg, float time, Color c) {
        m_text.text = msg;
        m_text.color = c;
        fadeTime = time;
        setup = true;
    }
}
