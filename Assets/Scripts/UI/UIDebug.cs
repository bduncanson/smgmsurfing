﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebug : MonoBehaviour {

    public Slider m_sliderRotationSpeed;
    public Text m_rotationSpeedVal;
    private float initRotationSpeedVal;

    public Slider m_sliderReturnRotationSpeed;
    public Text m_returnRotationSpeedVal;
    private float initReturnRotationSpeedVal;

    public Slider m_sliderVerticalForce;
    public Text m_verticalForceVal;
    private float initVerticalForceVal;

    public Slider m_sliderGravity;
    public Text m_gravityVal;
    private float initGravityVal;

    public Player m_player;
    

    void Start () {
        // Rotation Speed
        m_sliderRotationSpeed.onValueChanged.AddListener(delegate { RotationSpeedChange(); });
        initRotationSpeedVal = m_player.rotationModifier;
        m_sliderRotationSpeed.value = initRotationSpeedVal;
        m_rotationSpeedVal.text = m_sliderRotationSpeed.value.ToString("N2");

        // Rotation Return Speed
        m_sliderReturnRotationSpeed.onValueChanged.AddListener(delegate { RotationReturnSpeedChange(); });
        initReturnRotationSpeedVal = m_player.rotationSpeed;
        m_sliderReturnRotationSpeed.value = initReturnRotationSpeedVal;
        m_returnRotationSpeedVal.text = m_sliderReturnRotationSpeed.value.ToString("N2");

        // Vertical Force
        m_sliderVerticalForce.onValueChanged.AddListener(delegate { VerticalForceChange(); });
        initVerticalForceVal = m_player.vertMultiplier;
        m_sliderVerticalForce.value = initVerticalForceVal;
        m_verticalForceVal.text = m_sliderVerticalForce.value.ToString("N2");

        // Gravity
        m_sliderGravity.onValueChanged.AddListener(delegate { GravityChange(); });
        initGravityVal = m_player._initGravForce;
        m_sliderGravity.value = initGravityVal;
        m_gravityVal.text = m_sliderGravity.value.ToString("N3");
    }


    void RotationSpeedChange()
    {
        m_player.rotationModifier = m_sliderRotationSpeed.value;
        m_rotationSpeedVal.text = m_sliderRotationSpeed.value.ToString("N2");
    }

    void RotationReturnSpeedChange()
    {
        m_player.rotationSpeed = m_sliderReturnRotationSpeed.value;
        m_returnRotationSpeedVal.text = m_sliderReturnRotationSpeed.value.ToString("N2");
    }

    void VerticalForceChange()
    {
        m_player.vertMultiplier = m_sliderVerticalForce.value;
        m_verticalForceVal.text = m_sliderVerticalForce.value.ToString("N2");
    }

    void GravityChange()
    {
        m_player._initGravForce = m_sliderGravity.value;
        m_gravityVal.text = m_sliderGravity.value.ToString("N3");
    }

    public void ResetValues()
    {
        m_sliderRotationSpeed.value = initRotationSpeedVal;
        m_sliderReturnRotationSpeed.value = initReturnRotationSpeedVal;
        m_sliderVerticalForce.value = initVerticalForceVal;
    }

    public GameObject m_debugMenu;

    public void OpenCloseDebugMenu(bool open)
    {
        m_debugMenu.SetActive(open);
    }
}
